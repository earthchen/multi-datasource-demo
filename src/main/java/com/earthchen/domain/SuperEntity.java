package com.earthchen.domain;

import com.baomidou.mybatisplus.activerecord.Model;

import java.io.Serializable;

/**
 * 父类实体
 *
 * @param <T>
 */
public class SuperEntity<T extends Model> extends Model<T> {

    private static final long serialVersionUID = -4263419124367119871L;

    /**
     * 主键ID , 这里故意演示注解可以无
     */
    private Long id;


    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}