package com.earthchen;


import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * @author earthchen
 * @date 2018/8/26
 **/
public interface SuperMapper<T> extends BaseMapper<T> {

    // 这里可以放一些公共的方法
}
