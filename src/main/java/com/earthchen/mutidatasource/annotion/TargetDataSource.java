package com.earthchen.mutidatasource.annotion;

import java.lang.annotation.*;

/**
 * 多数据源标识
 *
 * @author earthchen
 * @date 2018/8/26
 **/
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface TargetDataSource {

    String name() default "";


}
