package com.earthchen.dao;

import com.earthchen.SuperMapper;
import com.earthchen.domain.User;

/**
 * @author earthchen
 * @date 2018/8/26
 **/
public interface UserDao extends SuperMapper<User> {


}
