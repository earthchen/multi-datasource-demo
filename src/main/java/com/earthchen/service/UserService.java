package com.earthchen.service;

import com.baomidou.mybatisplus.service.IService;
import com.earthchen.domain.User;

/**
 * user service
 *
 * @author earthchen
 * @date 2018/8/26
 **/
public interface UserService extends IService<User> {
}
