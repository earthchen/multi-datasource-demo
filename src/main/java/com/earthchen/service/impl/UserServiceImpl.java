package com.earthchen.service.impl;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.earthchen.dao.UserDao;
import com.earthchen.domain.User;
import com.earthchen.service.UserService;
import org.springframework.stereotype.Service;

/**
 * @author earthchen
 * @date 2018/8/26
 **/
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User>
        implements UserService {
}
