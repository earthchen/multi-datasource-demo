package com.earthchen.controller;

import com.earthchen.domain.User;
import com.earthchen.mutidatasource.annotion.TargetDataSource;
import com.earthchen.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author earthchen
 * @date 2018/8/26
 **/
@RestController
public class TestController {

    @Autowired
    private UserService userService;


    @GetMapping("/test")
    @TargetDataSource(name = "dataSource1")
    public String test() {
        User user = new User();
        user.setUsername("test1");
        user.setPassword("test1");
        userService.insert(user);
        return "ok";
    }


    @GetMapping("/test2")
    @TargetDataSource(name = "dataSource2")
    public String test2() {
        User user = new User();
        user.setUsername("test1");
        user.setPassword("test1");
        userService.insert(user);
        return "ok";
    }
}

